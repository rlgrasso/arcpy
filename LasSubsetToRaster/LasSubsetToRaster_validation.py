import arcpy
class ToolValidator(object):
    """Class for validating a tool's parameter values and controlling
    the behavior of the tool's dialog."""

    def __init__(self):
        """Setup arcpy and the list of tool parameters."""
        self.params = arcpy.GetParameterInfo()

    def initializeParameters(self):
        """Refine the properties of a tool's parameters.  This method is
        called when the tool is opened."""
        return

    def updateParameters(self):

        if not (self.params[1].value is None) and not self.params[1].hasBeenValidated:
            spatial_reference = arcpy.Describe(self.params[1].value).spatialReference
            if not (spatial_reference.name == "Unknown"):
                self.params[2].value = spatial_reference.factoryCode
        # condicion de multipoint
        if self.params[5].value:
            # raster habilitado
            self.params[7].enabled = True
        else:
            self.params[7].value = False
            self.params[7].enabled = False
            self.params[8].required = False

        if self.params[7].value:
            self.params[8].enabled = True
            self.params[9].enabled = True
        else:
            self.params[8].enabled = False
            self.params[9].enabled = False
        return

    def updateMessages(self):

        if self.params[4].value and (str(self.params[0].value) == str(self.params[3].value)):
            self.params[3].setErrorMessage("La carpeta debe ser distinta al origen de archivos .LAS en modo portable")

        if not (self.params[1].value is None):
            spatial_reference = arcpy.Describe(self.params[1].value).spatialReference
            if spatial_reference.name == "Unknown":
                self.params[1].setWarningMessage("El pointfile no tiene un sistema de referencias definido")

        if self.params[1].altered and not (self.params[1].value is None):
            if not "FILENAME" in [f.name.upper() for f in arcpy.ListFields(self.params[1].value)]:
                self.params[1].setErrorMessage("La tabla no tiene el campo 'FileName' requerido!")
        return
