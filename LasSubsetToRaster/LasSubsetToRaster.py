import sys

import arcpy
import glob
import os
from shutil import copyfile as copy

# Const
ds = '\\'
table_field_filename = "FILENAME"
table_field_spacing = "PT_SPACING"
table_field_value_raster = "SHAPE"
# project space default
# project_path = arcpy.env.scratchFolder

# Parameters
_las_folder_path = arcpy.GetParameterAsText(0)
_las_pointfile = arcpy.GetParameter(1)
_spatial_reference = arcpy.GetParameter(2)
_subset_folder = arcpy.GetParameterAsText(3)
_las_subset = arcpy.GetParameter(4)
_las_to_multipoint = arcpy.GetParameter(5)
_multipoint_class_codes = arcpy.GetParameter(6)
_point_to_raster = arcpy.GetParameter(7)
_raster_assignment_type = arcpy.GetParameter(8)
_raster_cell_size = arcpy.GetParameter(9)

# path a project_path del script
subset_path = _subset_folder

# Fuera del try-catch para evitar problemas con borrado de archivos
if _las_subset and (subset_path == _las_folder_path):
    arcpy.AddError("La carpeta de destino no puede ser la misma que la de archivos .LAS")
    raise arcpy.ExecuteError


def main():
    las_selection = None
    las_dataset = None
    las_filenames = []
    las_multipoint = None
    point_raster = None

    try:
        las_selection = getSelectionFromShape(_las_pointfile)

        if not (arcpy.GetCount_management(las_selection) > 0):
            arcpy.AddError("No se encuentra ninguna seleccion sobre el shape incluido")
            raise arcpy.ExecuteError
        arcpy.AddMessage("Se seleccionaron {0} celdas para el subset".format(arcpy.GetCount_management(las_selection)))

        spatial_reference = setSpatialReference()

        las_filenames = getDataFromTable(las_selection, table_field_filename)

        current_las_path = _las_folder_path
        if _las_subset:
            for las_filename in las_filenames:
                copyFile(current_las_path, subset_path, las_filename)
            current_las_path = subset_path

        las_files_abs = createAbsFilesFromFileList(current_las_path, las_filenames[:])

        las_dataset = createLasDataset(las_files_abs, spatial_reference)
        #arcpy.SetParameter(3, las_dataset)

        if _las_to_multipoint:

            las_multipoint = LasToMultipoint(las_selection, las_files_abs, spatial_reference)
            #arcpy.SetParameter(6, las_multipoint)

            if _point_to_raster:
                point_raster = pointToRaster(las_multipoint)
                arcpy.SetParameter(10, point_raster)

    except Exception:
        e = sys.exc_info()[1]
        if len(e.args) > 0:
            arcpy.AddError(e.args[0])
        arcpy.AddMessage("Rollback...")
        # rollback data files generated
        if _las_subset:
            deleteFilesfromFolder(subset_path, las_filenames)
        if las_dataset:
            arcpy.Delete_management(las_dataset)
        if las_multipoint:
            arcpy.Delete_management(las_multipoint)
        if point_raster:
            arcpy.Delete_management(point_raster)

    finally:
        arcpy.Delete_management(las_selection)

    return


def setSpatialReference():
    spatial_reference = _spatial_reference
    if spatial_reference is None:
        spatial_reference = arcpy.Describe(_las_pointfile).spatialReference
    return spatial_reference


def createAbsFilesFromFileList(path, filenames):
    # se setea el path absoluto a cada .las filename
    abs_filenames = []
    for filename in filenames:
        if checkPath(path, filename):
            abs_filenames.append(os.path.join(path, filename))
        else:
            arcpy.AddError("No existe el archivo .LAS {0} en la carpeta {1}".format(filename, path))
            raise arcpy.ExecuteError

    return abs_filenames


def checkPath(path, filename=None):
    abspath = path if filename is None else os.path.join(path, filename)
    if not os.path.exists(abspath):
        return False
    return True


def deleteFilesfromFolder(path_to_folder, filenames):
    for filename in filenames:
        if checkPath(path_to_folder, filename):
            for each_file in glob.glob(os.path.join(path_to_folder, filename + '*')):
                os.remove(each_file)


def getSelectionFromShape(shp_file):
    las_selection_name = arcpy.CreateUniqueName("las_selection", subset_path)
    las_selection = arcpy.CopyRows_management(shp_file, las_selection_name)
    return las_selection


def createLasDataset(las_filenames, spatial_reference):
    las_dataset_name = arcpy.CreateUniqueName("las_dataset.lasd", subset_path)
    retval = arcpy.CreateLasDataset_management(las_filenames, las_dataset_name, spatial_reference=spatial_reference)
    arcpy.AddMessage("Se ha creado LAS DATASET {0}".format(las_dataset_name))
    return retval


def LasToMultipoint(las_selection, las_filenames, spatial_reference):
    arcpy.AddMessage("Generando LAS MULTIPOINT...")
    # genera multipoint a partir de subset
    spacing_rows = getDataFromTable(las_selection, table_field_spacing)
    spacing_avg = round(sum(spacing_rows) / len(spacing_rows), 2)
    arcpy.AddMessage("Se va a utilizar el valor {0}pt como espaciado para el Multipoint".format(spacing_avg))
    lasMP = arcpy.CreateUniqueName('las_multipoint.shp', subset_path)
    retval = arcpy.LASToMultipoint_3d(las_filenames, lasMP, spacing_avg, _multipoint_class_codes,
                             input_coordinate_system=spatial_reference)
    arcpy.AddMessage("Se ha creado LAS MULTIPOINT {0}".format(lasMP))
    return retval


def pointToRaster(las_multipoint):
    # Si hay subset seleccionado asocia a raster con carpeta prj
    arcpy.AddMessage("Generando Raster...")

    # TODO: Validación de SHAPE.Z
    # if table_field_value_raster in [f.name.upper() for f in arcpy.ListFields(las_multipoint)]:
    #     c = arcpy.SearchCursor(las_multipoint)
    #     row.getValue(table_field_value_raster)
    #     arcpy.AddMessage(arcpy.Describe(val))
    #     if not row:
    #         arcpy.AddError("El Mulitpoint no tiene profundidad Z para seleccionar")

    out_raster = arcpy.CreateUniqueName('pointToRaster.tif', subset_path)
    cell_size = None if _raster_cell_size == 0 else _raster_cell_size
    raster = arcpy.PointToRaster_conversion(las_multipoint, out_rasterdataset=out_raster,
                                            cell_assignment=_raster_assignment_type,
                                            cellsize=cell_size, value_field='SHAPE.Z')
    arcpy.AddMessage("Se ha creado Raster {0}".format(out_raster))

    return raster


def copyFile(src, dst, filename, wildcard='*'):
    if not checkPath(os.path.join(src, filename)):
        arcpy.AddError("El archivo {0} no existe en {1}".format(filename, src))
        raise arcpy.ExecuteError

    if not checkPath(dst):
        arcpy.AddError("No se encuentra la ubicacion {0} para copiar el archivo {1}".format(dst, filename))
        raise arcpy.ExecuteError

    for each_file in glob.glob(os.path.join(src, filename + wildcard)):
        current_filename = each_file.split(ds)[-1]
        copy(each_file, os.path.join(dst, current_filename))
        arcpy.AddMessage("Se transfiere el archivo {0} a {1} ".format(current_filename, dst))


def getDataFromTable(table, field):
    fieldnames = [f.name.upper() for f in arcpy.ListFields(table)]
    if field not in fieldnames:
        arcpy.AddError("La tabla de origen no tiene el campo {0} solicitado".format(field))
        raise arcpy.ExecuteError

    cursor = arcpy.SearchCursor(table)
    data = []
    for row in cursor:
        data.append(row.getValue(field))
    return data


main()

