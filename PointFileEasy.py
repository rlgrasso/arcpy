import arcpy

# Const
ds = '\\'
# Es el primer campo tambien
table_field = "FILE_NAME"
# project space default
project_path = arcpy.env.scratchFolder

# Parameters
_las_dataset = arcpy.GetParameter(0)


def main():
    arcpy.AddMessage("Leyendo info del dataset...")

    spatial_reference = arcpy.Describe(_las_dataset).spatialReference

    txt_name = arcpy.CreateUniqueName("temp.txt", project_path)
    txt_result = arcpy.LasDatasetStatistics_management(_las_dataset, out_file=txt_name, summary_level='LAS_FILES')

    arcpy.AddMessage("Generando lista de LAS files...")

    try:
        las_filenames = getUniqueDataFromTable(txt_result, table_field)

        if not (len(las_filenames) > 0):
            arcpy.AddError("No se encontraron los archivos LAS asociados al Dataset")
            raise arcpy.ExecuteError

        arcpy.AddMessage("Generando shapefile de {0} celdas".format(len(las_filenames)))
        pointfile_name = arcpy.CreateUniqueName("las_pointfile.shp", project_path)
        result = arcpy.PointFileInformation_3d(las_filenames, pointfile_name, 'LAS',
                                               input_coordinate_system=spatial_reference)
        arcpy.SetParameter(1, result)
    finally:
        arcpy.Delete_management(txt_result)


def getUniqueDataFromTable(table, field):
    fieldnames = [f.name.upper() for f in arcpy.ListFields(table)]
    # excepcion si no hay File_Name
    if field not in fieldnames:
        arcpy.AddError("La tabla de origen no tiene el campo {0} requerido".format(field))
        raise arcpy.ExecuteError

    cursor = arcpy.SearchCursor(table)
    data = []
    for row in cursor:
        filename = row.getValue(field)
        if filename not in data:
            arcpy.AddMessage("Incluyendo Archivo {0}".format(filename))
            data.append(filename)
    return data


main()
