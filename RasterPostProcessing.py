import arcpy
from arcpy import env
from arcpy.sa import Con, IsNull, FocalStatistics
import os

# Todo: outputfile = Con(IsNull("point2ras"), FocalStatistics("point2ras", NbrRectangle(3,3,"CELL"),"MEAN", "DATA"),"point2ras")
# arcpy.gp.RasterCalculator_sa("""Con(IsNull("pointToRaster.tif"), FocalStatistics("pointToRaster.tif", NbrRectangle(3,3,"CELL"),"MEAN", "DATA"),"pointToRaster.tif")""","C:/Users/rleandro/Documents/ArcGIS/Default.gdb/rastercalc")

_raster = arcpy.GetParameter(0)
_neighborhood = arcpy.GetParameter(1)
_statistics_type = arcpy.GetParameter(2)


def main():

    raster = _raster
    null_raster = arcpy.gp.IsNull_sa(raster)
    no_data_count = getAssociatedValueFromTable(null_raster, "COUNT", "VALUE = 1")
    arcpy.AddMessage("{0} NoData restantes".format(no_data_count))
    while no_data_count > 0:
        raster = arcpy.gp.Con_sa(null_raster,
                              arcpy.gp.FocalStatistics_sa(raster, "#", _neighborhood, _statistics_type, "DATA"), "#",
                              raster)
        no_data_count = getAssociatedValueFromTable(raster, "COUNT", "VALUE = 1")
        null_raster = arcpy.gp.IsNull_sa(raster)
        arcpy.AddMessage("{0} NoData restantes".format(no_data_count))

    arcpy.SetParameter(3, raster)


def getAssociatedValueFromTable(table, field, where_clause):
    fieldnames = [f.name.upper() for f in arcpy.ListFields(table)]
    if field not in fieldnames:
        arcpy.AddError("La tabla de origen no tiene el campo {0} solicitado".format(field))
        raise arcpy.ExecuteError

    cursor = arcpy.SearchCursor(table, where_clause)
    data = []
    for row in cursor:
        data.append(row.getValue(field))

    if len(data) > 1:
        arcpy.AddError("Error interno. La seleccion del valor retorna mas de 1 fila")
        raise arcpy.ExecuteError

    if len(data) == 0:
        return 0

    return data[0]


main()

